var express = require('express');

var app = express();

var port = 5000;

//Static resources e.g. CSS, JS
app.use(express.static('public'));
app.use(express.static('src/views'));


//Handle routing request
app.get('/', function(req, res){
    res.send('Hello, World');
});

app.get('/books', function(req, res){
    res.send('Hello, Books');
});

app.get('/library', function(req, res){
    res.send('Hello, Library');
});

//Start the server listening on port number
app.listen(port, function(err){
    console.log('running server on port: ' + port);
});